import { IfStmt } from '@angular/compiler';
import { Component } from '@angular/core';
import { Country } from '../../interfaces/pais-interface';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-por-region',
  templateUrl: './por-region.component.html',
  styleUrls: ['./por-region.component.css']
})
export class PorRegionComponent {

  regiones: string[] = 
[
  'EU',
  'EFTA',
  'CARICOM',
  'PA', 
  'AU', 
  'USAN', 
  'EEU', 
  'AL', 
  'ASEAN', 
  'CAIS', 
  'CEFTA', 
  'NAFTA', 
  'SAARC'
]
  regionActiva: string = ''


  termino: string = ''
  paises: Country[] = []
  hayError: boolean = false

  constructor( private PaisService: PaisService){

  }
  activarRegion( region : string){

    if( region === this.regionActiva) return 

    this.regionActiva = region
    this.paises = []
    this.PaisService.buscarRegion( region )
    .subscribe( paises => this.paises = paises)

  }

  getClaseCSS( region: string ): string{
    return (region === this.regionActiva) ? 'btn btn-primary' : 'btn btn-outline-primary'
  } 
  
  buscar( termino: string ){
    this.hayError = false
    this.termino = termino
    
    this.PaisService.buscarPais( this.termino )
    .subscribe( paises =>{
      this.paises = paises
      console.log( paises )
    }, (err) =>
    {
      this.hayError = true
      this.paises = []
    }) 
  }

}

